import React, { Component } from 'react';
import styled from 'styled-components';
import RectangleResult from './rectangleResult';

const MyRectangle = styled.div`
width:${props => props.width + 'px'};
height:${props => props.height + 'px'};
background-color:black;
display:flex;
text-align: center;
align-items:center;
justify-content: center;
transition: all 0.5s;
margin: auto;
`;

class Rectangle extends Component {
    constructor() {
        super()
        this.state = {
            width: 100,
            height: 120,
            max: 500,
            min: 100
        }
    }

    handleResize = () => {
        const min = this.state.min;
        const max = this.state.max;
        let width = Math.floor(Math.random() * (max - min) + min);
        let height = Math.floor(Math.random() * (max - min) + min);
        this.setState({
            width,
            height
        })
    }

    render() {
        let { width, height } = this.state;
        const ref = React.createRef();
        return (
            <div className="rectangle__wrapper">
                <MyRectangle ref={ref} width={width} height={height}>
                     <RectangleResult width={width} height={height} />
                </MyRectangle>
                <p>Szerokość: {width} px</p>
                <p>Wyskość:{height} px</p>
                <button onClick={() => this.handleResize()}>Generuj nowy element</button>
            </div>
        )
    }
}

export default Rectangle;
