import React, { useState } from 'react';
import styled from 'styled-components';
import RectangleResult from './rectangleResult';

const MyRectangle = styled.div`
width:${props => props.width + 'px'};
height:${props => props.height + 'px'};
background-color:black;
display:flex;
text-align: center;
align-items:center;
justify-content: center;
transition: all 0.5s;
margin: auto;
`;

const Rectangle = () => {
    const [values, setValues] = useState({
        width: '100',
        height: '120',
        max: '500',
        min: '100'
    });

    let width = values.width;
    let height = values.height;
    let min = values.min;
    let max = values.max;
    const ref = React.createRef();

    const handleResize = e => {
        width = Math.floor(Math.random() * (max - min) + min);
        height = Math.floor(Math.random() * (max - min) + min);
        setValues({ width: width, height: height, min: min, max: max });
    }

    return (
        <div className="rectangle__wrapper">
            <MyRectangle ref={ref} width={width} height={height}>
                <RectangleResult width={width} height={height} />
            </MyRectangle>
            <p>Szerokość: {width} px</p>
            <p>Wyskość:{height} px</p>
            <button onClick={handleResize}>Generuj nowy element</button>
        </div>
    );
}
export default Rectangle;